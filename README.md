README
======

## Getting Started

* Install [node.js](http://nodejs.org/)

open the terminal where the package.json file is and type npm install


======================================
========== For Windows Users =========
======================================

* Install [node.js](http://nodejs.org/)
* Install [python](https://www.python.org/) - I recommend downloading 32bit regardless of your system

open CMD where the package.json file is and type:
	> npm install

When that is done type:
	> gulp

The default gulp task is "watch".

