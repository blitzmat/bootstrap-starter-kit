'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var size = require('gulp-size');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');


var sources = {
	scss: 'src/scss/**/*.scss',
	images: '../img/**/*.{jpg|png|gif}',
	sprites: '../img/sprite-src/*.png'
}

/**
 * Compiles SASS into CSS.
 */
gulp.task('scss', function () {
	return gulp.src([sources.scss])
		.pipe( sass({
			includePaths:['node_modules/bootstrap-sass/assets/stylesheets'],
			outputStyle: 'nested', // 'nested', 'compressed'
			sourceComments: 'normal', // 'none', 'normal', 'map'
			errLogToConsole: true, 
		}))
		.pipe( autoprefixer('last 4 versions', '> 5%', 'ie 8') )
		.pipe( gulp.dest('../css') )
		.pipe( size({title: 'styles:scss'}) );
});

/**
 * Create spritesheet
 * Combines multiple .png's images into a single .png image. Outputs a .scss file with
 * corresponding variables for each image in the spritesheet.
 */
gulp.task('sprite', function () {
	var spriteData = gulp.src(sources.sprites)
		.pipe( spritesmith({
			cssName: '_sprite.scss',
			cssFormat: 'css', // use .scss to get sprite generator mixin
			imgName: 'sprite.png',
			imgPath: '../img/sprite.png', // the path our css will reference
			algorithm: 'binary-tree',
			padding: 1, // prevents pixel rounding issues when we use CSS transforms on sprites
			cssOpts: {
				cssClass: function (item) {
					return '.sprite-' + item.name;
				}
			}
		}));

	// Optimize and output the generated spritesheet image
	spriteData.img
		.pipe( imagemin() )
		.pipe( gulp.dest('../img/') )
		.pipe( size({title: 'sprite'}) );

	// Output the generated .scss file
	spriteData.css
		.pipe( gulp.dest('src/scss/helper/') );  
});

// Re-run these tasks when a file changes
gulp.task('watch', function() {

	gulp.watch(sources.scss, ['scss']);

	// Because our sprite task generates a new scss file, it will trigger the watched scss task
	gulp.watch(sources.sprites, ['sprite']);

});

gulp.task('default', ['watch']);